/* run2_gpl -- Core routines for the run2 wrapper which are GPL
 * Copyright (C) 1998,2009  Charles S. Wilson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#ifndef RUN2_GPL_H
#define RUN2_GPL_H

void run2_setup_win_environ (void);
BOOL run2_have_console (void);
BOOL run2_target_is_gui (const char* target_path);
BOOL run2_setup_invisible_console (void);
BOOL run2_configure_startupinfo (STARTUPINFO * psi, BOOL bHaveConsole,
                                 BOOL bForceUsingPipes, BOOL * bUsingPipes,
                                 HANDLE * hpToChild, HANDLE * hpFromChild,
                                 HANDLE * hpToParent, HANDLE * hpFromParent);
DWORD run2_start_child (char *cmdline, char *startin, int wait_for_child);

#endif

