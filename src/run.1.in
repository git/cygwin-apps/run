.TH RUN 1 September\ 2014 Version\ @VERSION@ run\ @VERSION@
.SH NAME
run - start programs with hidden console window

.SH SYNOPSIS
.B run
[ -p path ] command [ --wait ] [ --quote ] arguments

.B runcommand
[ -p path ] [ --wait ] [ --quote ] arguments

.SH DESCRIPTION
Windows programs are either GUI programs or console programs. When  started
console  programs  will  either  attach  to an existing console or create a 
new one. GUI programs can  never attach to an exiting console. There is no way
to attach to an existing console but hide it if started as GUI program.
  
.I run
will do this for you. It works  as intermediate and starts a program but makes
the console window hidden.

With 
.B -p path
you can add 
.B path
to the PATH environment variable.

Issuing 
.B --wait
as first program  argument will make 
.I run 
wait for program completition, otherwise it returns immediately.

The
.B --quote
option enables automatic quoting of arguments with embedded
whitespace, double quotes or backslashes for consumption by the
standard Microsoft startup code or compatible implementations.  If you
do not use this option,
.I run
assumes that the arguments are already correctly quoted for the
consumer and simply concatenates them separated by single spaces.

The second variant is for  creating wrappers. If the executable is named
.B runcommand 
(eg runemacs), 
.I run
will try  to start the program (eg emacs).

.SH NOTES

For backwards compatibility, the long options
.B -wait
and
.B -quote
can be issued with a single leading dash.

.SH EXAMPLES
run -p /usr/X11R6/bin xterm

run emacs -wait
.br
runemacs -wait

run make -wait

.SH AUTHORS
Charles S. Wilson

Harold L Hunt II 

Jehan Bing 

Alexander Gottwald 

