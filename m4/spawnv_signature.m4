# RUN_FUNC_SPAWNV_ARGTYPE3
# -----------------------
# Determine the correct type to be passed as the third argument of
# the spawnv() function, and define that type in `SPAWNV_TYPE_ARG3'.
AC_DEFUN([RUN_FUNC_SPAWNV_ARGTYPE3],
[AC_CHECK_HEADERS(process.h)
AC_CACHE_CHECK([type of argument for spawnv],
[run_cv_func_spawnv_arg],
[for ac_arg3 in 'const char * const *' 'char * const *' 'char **' ; do
   AC_COMPILE_IFELSE(
       [AC_LANG_PROGRAM(
[AC_INCLUDES_DEFAULT
#ifdef HAVE_PROCESS_H
# include <process.h>
#endif
],
[extern int spawnv (int, const char*, $ac_arg3);])],
[run_cv_func_spawnv_arg="$ac_arg3"; break])
done
# Provide a safe default value.
: "${run_cv_func_spawnv_arg=const char * const *}"
])
run_save_IFS=$IFS; IFS=','
set dummy `echo "$run_cv_func_spawnv_arg" | sed 's/\*/\*/g'`
IFS=$run_save_IFS
shift
AC_DEFINE_UNQUOTED(SPAWNV_TYPE_ARG3, $[1],
                   [Define to the type of arg 3 for `spawnv'.])# '`
rm -f conftest*
])# RUN_FUNC_SPAWNV_ARGTYPE3


# RUN_FUNC__SPAWNV_ARGTYPE3
# -----------------------
# Determine the correct type to be passed as the third argument of
# the _spawnv() function, and define that type in `_SPAWNV_TYPE_ARG3'.
AC_DEFUN([RUN_FUNC__SPAWNV_ARGTYPE3],
[AC_CHECK_HEADERS(process.h)
AC_CACHE_CHECK([type of argument for _spawnv],
[run_cv_func__spawnv_arg],
[for ac_arg3 in 'const char * const *' 'char * const *' 'char **' ; do
   AC_COMPILE_IFELSE(
       [AC_LANG_PROGRAM(
[AC_INCLUDES_DEFAULT
#ifdef HAVE_PROCESS_H
# include <process.h>
#endif
],
[extern int _spawnv (int, const char*, $ac_arg3);])],
[run_cv_func__spawnv_arg="$ac_arg3"; break])
done
# Provide a safe default value.
: "${run_cv_func__spawnv_arg=const char * const *}"
])
run_save_IFS=$IFS; IFS=','
set dummy `echo "$run_cv_func__spawnv_arg" | sed 's/\*/\*/g'`
IFS=$run_save_IFS
shift
AC_DEFINE_UNQUOTED(_SPAWNV_TYPE_ARG3, $[1],
                   [Define to the type of arg 3 for `_spawnv'.])#'`
rm -f conftest*
])# RUN_FUNC__SPAWNV_ARGTYPE3

